/* UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS
 * Facultad de Ingeniería de Sistemas e Informática
 * E.A.P. de Ingeniería de Software
 * Curso: Estructura de Datos
 * Laboratorio: XXX - EjercicioXX
 * Descripción:
 *
 * Autor: Grupo 8
 *
*/

#include <iostream>
#include <string>

struct tNodo;
typedef tNodo* tLista; // tLista es un puntero a un nodo
struct tNodo{
	char dato;
	tLista siguiente;
};

// PROTOTIPOS DE LAS FUNCIONES

int menu();

// Operaciones compartidas
tLista crearNodo(char dato);
void mostrarLista(tLista lista);

// Operaciones basicas con las colas
void encolar(tLista &lista, char dato);
void decolar(tLista &lista);
void eliminarElemento(tLista &lista,int pos);

using namespace std;

int main(){
	
	int opc,pos;
	char dato;
	
	tLista cola = NULL;
	
	opc = menu();
	
	while(opc!=0){
		switch(opc){
			case 1:
				cout<<"Ingrese valor: "; cin>>dato;
				encolar(cola,dato);
				mostrarLista(cola);
				break;
			case 2:
				decolar(cola);
				mostrarLista(cola);
				break;
			case 3:
				cout<<"Ingrese posicion: "; cin>>pos;
				eliminarElemento(cola,pos);
				mostrarLista(cola);
				break;	
			case 4:
				mostrarLista(cola);
				break;	
		}
		opc = menu();
	}
	
	
	cin.get();
}

int menu() {

    int opc;
    
    cout<<":::::: MENU ::::::"<<endl;
	cout<<"[1] Encolar"<<endl;
	cout<<"[2] Decolar"<<endl;
	cout<<"[3] Eliminar elemento de cola"<<endl;
	cout<<"[4] Mostrar"<<endl;
	cout<<"[0] Salir"<<endl;
    
    do{
		cout<<"OPCION: ";
		cin>>opc;	
	}while(opc<0 || 4<opc);
		
    return  opc;
}


tLista crearNodo(char dato){
	tLista nuevo = new tNodo; // new tNodo devuelve una direccion que se almacena en un puntero
	nuevo->dato = dato;
	nuevo->siguiente = NULL;
	
	return nuevo;	
}

void mostrarLista(tLista lista){
	tLista p = lista;
	
	while(p != NULL){
		cout<<p->dato<<"->";
		p = p->siguiente;
	}
	cout<<"NULL"<<endl<<endl;
}

// Operaciones basicas con las colas

void encolar(tLista &lista, char dato){
	tLista nuevo = crearNodo(dato);
	
	if(lista == NULL){		
		lista = nuevo;
	}else{
		tLista p = lista;
		while(p->siguiente != NULL){
			p = p->siguiente;
		}
		p->siguiente = nuevo;	
	}
	
}

void decolar(tLista &lista){
	
	if(lista == NULL){
		cout<<"Cola vacia!";
	}else{
		tLista temporal = lista;
        lista = lista->siguiente;
        delete temporal;
	}        	
}

void eliminarElemento(tLista &lista,int pos){
	char letra;
	int n=1;
	tLista p=lista, nueva=NULL;
	while(n!=pos){
		n++;
		encolar(nueva,p->dato);
		decolar(p);
	}
	decolar(p);
	
	while(p!=NULL){
		encolar(nueva,p->dato);
		decolar(p);
	}
	lista = nueva;
}

