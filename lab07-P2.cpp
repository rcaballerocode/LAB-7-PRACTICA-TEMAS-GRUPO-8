/* UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS
 * Facultad de Ingeniería de Sistemas e Informática
 * E.A.P. de Ingeniería de Software
 * Curso: Estructura de Datos
 * Laboratorio: XXX - EjercicioXX
 * Descripción:
 *
 * Autor: Grupo 8
 *
*/

#include <iostream>
#include <string>

struct tNodo;
typedef tNodo* tLista; // tLista es un puntero a un nodo
struct tNodo{
	int dato;
	tLista anterior;
	tLista siguiente;
};

struct tNodoSimple;
typedef tNodoSimple* tListaSimple;
struct tNodoSimple{
	int dato;
	tListaSimple siguiente;
};


// PROTOTIPOS DE LAS FUNCIONES

int menu();

// Operaciones basicas con listas
tLista crearNodo(int dato);
tListaSimple crearNodoSimple(int dato);

void recorrerLista(tLista lista);
void recorrerListaSimple(tListaSimple lista);
void insertarFinal(tLista &lista, int dato);

void procesar(tLista lista,tListaSimple &listaCircular,tListaSimple &listaSimple);

void addListaCircular(tListaSimple &listaCircular,int dato);
void insertarListaCircular(tListaSimple &listaCircular,int dato);
void recorrerListaCircular(tListaSimple listaCircular);

void addListaSimple(tListaSimple &listaSimple,int dato);
void insertarFinalSimple(tListaSimple &lista, int dato);

using namespace std;

int main(){
	
	int opc,dato,pos;
	
	tLista lista = NULL; // Esta es mi lista doblemente enlazada
	tListaSimple listaCircular = NULL; // Esta es mi lista doblemente enlazada circular
	tListaSimple listaSimple = NULL; // Esta es mi lista simple
	
	opc = menu();
	
	while(opc!=0){
		switch(opc){
			case 1:
				cout<<"Ingrese valor: "; cin>>dato;
				insertarFinal(lista,dato); // Agregando datos a la lista doble
				recorrerLista(lista);
				break;
			case 2:
				procesar(lista,listaCircular,listaSimple);
				cout<<"Lista simple con los datos mayores a la media"<<endl;
				recorrerListaSimple(listaSimple);
				cout<<"Lista simple con los datos menores a la media"<<endl;
				recorrerListaCircular(listaCircular);
				break;		
			case 3:
				recorrerLista(lista);
				break;	
	
		}
		opc = menu();
	}
	
	
	cin.get();
}

int menu() {

    int opc;
    
    cout<<":::::: MENU ::::::"<<endl;
	cout<<"[1] Adicionar elemento"<<endl;
	cout<<"[2] Procesar"<<endl;
	cout<<"[3] Mostrar"<<endl;
	cout<<"[0] Salir"<<endl;
    
    do{
		cout<<"OPCION: ";
		cin>>opc;	
	}while(opc<0 || 3<opc);
		
    return  opc;
}

tLista crearNodo(int dato){
	tLista nuevo = new tNodo; // new tNodo devuelve una direccion que se almacena en un puntero
	nuevo->dato = dato;
	nuevo->anterior = NULL;
	nuevo->siguiente = NULL;
	
	return nuevo;	
}

tListaSimple crearNodoSimple(int dato){
	tListaSimple nuevo = new tNodoSimple; // new tNodo devuelve una direccion que se almacena en un puntero
	nuevo->dato = dato;
	nuevo->siguiente = NULL;
	
	return nuevo;	
}


void recorrerLista(tLista lista){
	tLista p = lista;
	
	while(p != NULL){
		cout<<p->dato<<"->";
		p = p->siguiente;
	}
	cout<<"NULL"<<endl<<endl;
}

void recorrerListaSimple(tListaSimple lista){
	tListaSimple p = lista;
	
	while(p != NULL){
		cout<<p->dato<<"->";
		p = p->siguiente;
	}
	cout<<"NULL"<<endl<<endl;
}

void recorrerListaCircular(tListaSimple listaCircular){
	tListaSimple p = listaCircular;
	
	while(p->siguiente != listaCircular){
		cout<<p->dato<<"->";
		p = p->siguiente;
	}
	cout<<p->dato<<"->";
	//cout<<"NULL"<<endl<<endl;
}

// Algoritmos de Insercion

void insertarFinal(tLista &lista, int dato){
	tLista nuevo = crearNodo(dato);
	
	if(lista == NULL){		
		lista = nuevo;
	}else{
		tLista p = lista;
		while(p->siguiente != NULL){
			p = p->siguiente;
		}
		p->siguiente = nuevo;
		nuevo->anterior = p;	
	}
	
}

void procesar(tLista lista,tListaSimple &listaCircular,tListaSimple &listaSimple){
	tLista p = lista;
	int sum=0;
	int contador=1;
	double prom=0;
	
	while(p!=NULL){
		sum = sum + p->dato;
		p = p->siguiente;
		contador++;
	}
	
	prom = sum/contador;
	
	p = lista;
	
	while(p!=NULL){
		if(p->dato<=prom){
			addListaCircular(listaCircular,p->dato);
		}else{
			addListaSimple(listaSimple,p->dato);
		}
		p = p->siguiente;
	}
	
}


void addListaCircular(tListaSimple &listaCircular,int dato){
	insertarListaCircular(listaCircular,dato);
}

void insertarListaCircular(tListaSimple &listaCircular,int dato){
	
	tListaSimple nuevo = crearNodoSimple(dato);
	
	if(listaCircular == NULL){		
		listaCircular = nuevo;
		nuevo->siguiente=nuevo;
	}else{
		tListaSimple p = listaCircular;
		if(p->siguiente == listaCircular){
			listaCircular->siguiente = nuevo;
			nuevo->siguiente = listaCircular;			
		}else{
			//tListaSimple p = listaCircular;
			while(p->siguiente != listaCircular){
				p = p->siguiente;
			}
			p->siguiente = nuevo;	
			nuevo->siguiente=listaCircular;
		}
		
	}
	
}


void addListaSimple(tListaSimple &listaSimple,int dato){
	insertarFinalSimple(listaSimple,dato);
}


void insertarFinalSimple(tListaSimple &lista, int dato){
	tListaSimple nuevo = crearNodoSimple(dato);
	
	if(lista == NULL){		
		lista = nuevo;
	}else{
		tListaSimple p = lista;
		while(p->siguiente != NULL){
			p = p->siguiente;
		}
		p->siguiente = nuevo;	
	}
	
}




