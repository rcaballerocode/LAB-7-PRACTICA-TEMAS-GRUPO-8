#include <iostream>
#include <string>

struct tNodo;
typedef tNodo* tLista; // tLista es un puntero a un nodo
struct tNodo{
	int dato;
	tLista siguiente;
};

int menu();

// Operaciones basicas con listas
tLista crearNodo(int dato);
int obtenerDatoNodo(tLista lista, int pos);
void cambiarDatoNodo(tLista &lista, int pos, int dato);
int sizeLista(tLista lista);
void recorrerLista(tLista lista);

// Algoritmos de Insercion
void insertarInicio(tLista &lista, int dato);
void insertarFinal(tLista &lista, int dato);
void insertarPosicion(tLista &lista,int dato, int pos);

// Algoritmos de Ordenamiento

void ordenamientoShell(tLista &lista, int size);

using namespace std;

int main(){
	
	int opc,dato,pos;
	
	tLista lista = NULL;
	
	opc = menu();
	
	while(opc!=0){
		switch(opc){
			case 1:
				cout<<"Ingrese valor: "; cin>>dato;
				insertarInicio(lista,dato);
				recorrerLista(lista);
				break;
			case 2:
				ordenamientoShell(lista,sizeLista(lista));
				//recorrerLista(lista);
				break;		
		}
		opc = menu();
	}
	
	
	cin.get();
}

int menu() {

    int opc;
    
    cout<<":::::: MENU ::::::"<<endl;
	cout<<"[1] Adicionar al inicio"<<endl;
	cout<<"[2] Ordenar Shell"<<endl;
	cout<<"[0] Salir"<<endl;
    
    do{
		cout<<"OPCION: ";
		cin>>opc;	
	}while(opc<0 || 2<opc);
		
    return  opc;
}

tLista crearNodo(int dato){
	tLista nuevo = new tNodo; // new tNodo devuelve una direccion que se almacena en un puntero
	nuevo->dato = dato;
	nuevo->siguiente = NULL;
	
	return nuevo;	
}

int obtenerDatoNodo(tLista lista, int pos){
	tLista p = lista;
	int contador = 1;
	while(contador < pos && p->siguiente!=NULL){
		p = p->siguiente;
		contador++;
	}
	return p->dato;
	
}

void cambiarDatoNodo(tLista &lista, int pos, int dato){
	tLista p = lista;
	int contador = 1;
	while(contador < pos && p->siguiente!=NULL){
		p = p->siguiente;
		contador++;
	}
	p->dato = dato;
}

int sizeLista(tLista lista){
	tLista p = lista;
	if(p==NULL){
		return 0;
	}else{
		int contador = 1;
		while(p->siguiente!=NULL){
			p = p->siguiente;
			contador++;
		}
		return contador;
	}	
	
}

void recorrerLista(tLista lista){
	tLista p = lista;
	
	while(p != NULL){
		cout<<p->dato<<"->";
		p = p->siguiente;
	}
	cout<<"NULL"<<endl<<endl;
}

// Algoritmos de Insercion

void insertarInicio(tLista &lista, int dato){
	tLista nuevo = crearNodo(dato);
	
	if(lista == NULL){		
		lista = nuevo;
	}else{
		nuevo->siguiente = lista;
		lista = nuevo;
	}
}


void ordenamientoShell(tLista &lista, int size){
	int intervalo,i,j,k;
	intervalo = size/2;
	
	while(intervalo > 0){
		for(i = intervalo+1;i<=size;i++){
			j = i - intervalo;
			while(j>0){
				k = j + intervalo;
				if(obtenerDatoNodo(lista,j)<=obtenerDatoNodo(lista,k)){
					j = -1;
				}else{
					int aux;
					aux = obtenerDatoNodo(lista,j);
					cambiarDatoNodo(lista,j,obtenerDatoNodo(lista,k));
					cout<<"Intercambio "<<aux<<" con "<<obtenerDatoNodo(lista,j)<<" ";
					cambiarDatoNodo(lista,k,aux);
					j = j - intervalo;
					recorrerLista(lista);
					
				}
			}
		}
		intervalo = intervalo/2;
	}
}
