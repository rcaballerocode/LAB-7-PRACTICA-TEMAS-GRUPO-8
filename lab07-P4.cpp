/* UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS
 * Facultad de Ingeniería de Sistemas e Informática
 * E.A.P. de Ingeniería de Software
 * Curso: Estructura de Datos
 * Laboratorio: XXX - EjercicioXX
 * Descripción:
 *
 * Autor: Grupo 8
 *
*/

#include <iostream>
#include <string>

struct tNodo;
typedef tNodo* tLista; // tLista es un puntero a un nodo
struct tNodo{
	char dato;
	tLista siguiente;
};

// PROTOTIPOS DE LAS FUNCIONES

int menu();

// Operaciones basicas con listas
tLista crearNodo(char dato);
void insertarFinal(tLista &lista, char dato);
void procesar(tLista &lista);
void recorrerLista(tLista lista);

using namespace std;

int main(){
	
	int opc,pos;
	char dato;
	
	tLista lista = NULL;
	
	opc = menu();
	
	while(opc!=0){
		switch(opc){
			case 1:
				cout<<"Ingrese dato: "; cin>>dato;
				insertarFinal(lista,dato);
				recorrerLista(lista);
				break;
			case 2:
				procesar(lista);
				recorrerLista(lista);
				break;
			case 3:
				recorrerLista(lista);
				break;	
		}
		opc = menu();
	}
	
	
	cin.get();
}

int menu() {

    int opc;
    
    cout<<":::::: MENU ::::::"<<endl;
	cout<<"[1] Adicionar elemento"<<endl;
	cout<<"[2] Procesar"<<endl;
	cout<<"[3] Mostrar lista"<<endl;
	cout<<"[0] Salir"<<endl;
    
    do{
		cout<<"OPCION: ";
		cin>>opc;	
	}while(opc<0 || 3<opc);
		
    return  opc;
}

tLista crearNodo(char dato){
	tLista nuevo = new tNodo; // new tNodo devuelve una direccion que se almacena en un puntero
	nuevo->dato = dato;
	nuevo->siguiente = NULL;
	
	return nuevo;	
}

void recorrerLista(tLista lista){
	tLista p = lista;
	
	while(p != NULL){
		cout<<p->dato<<"->";
		p = p->siguiente;
	}
	cout<<"NULL"<<endl<<endl;
}

void insertarFinal(tLista &lista, char dato){
	tLista nuevo = crearNodo(dato);
	
	if(lista == NULL){		
		lista = nuevo;
	}else{
		tLista p = lista;
		while(p->siguiente != NULL){
			p = p->siguiente;
		}
		p->siguiente = nuevo;	
	}
	
}

void procesar(tLista &lista){
	tLista p = lista;
	char arreglo[5];
	int i=0;
	
	while(p!=NULL){
		arreglo[i] = p->dato;
		p = p->siguiente;
		i++;
	}
	
	p = lista;
	
	p->dato = arreglo[4];
	p = p->siguiente;
	p->dato = arreglo[3];
	p = p->siguiente;
	p->dato = arreglo[0];
	p = p->siguiente;
	p->dato = arreglo[2];
	p = p->siguiente;
	p->dato = arreglo[1];

}
