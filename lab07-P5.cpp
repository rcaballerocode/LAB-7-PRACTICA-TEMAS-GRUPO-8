#include <iostream>
#include <string>

struct tNodo;
typedef tNodo* tLista; // tLista es un puntero a un nodo
struct tNodo{
	char dato;
	tLista ant;
	tLista sig;	
};

// PROTOTIPOS DE LAS FUNCIONES

int menu();
tLista crearNodo(int dato);

void insertarInicio(tLista &lista, char dato);
void insertarFinal(tLista &lista, char dato);
void insertarPosicion(tLista &lista, char dato, int pos);
void recorrerLista(tLista lista,bool aux);
int obtenerPosicion(tLista lista, char dato);

using namespace std;

int main(){


	int opc,pos;
	char dato;
	bool a=false;
	bool b=false;
	
	tLista lista = NULL;
	
	insertarFinal(lista,'N');
	insertarFinal(lista,'M');
	insertarFinal(lista,'L');
	recorrerLista(lista,false);
	
	opc = menu();
	
	while(opc!=0){
		switch(opc){
			case 1:
				cout<<"Insertando el nodo A en la primera posicion"<<endl;
				insertarInicio(lista,'A');
				recorrerLista(lista,false);
				break;
			case 2:
				if(a==false){
					cout<<"Insertando el nodo I entre N y M"<<endl;
					pos = obtenerPosicion(lista,'M');
					insertarPosicion(lista,'I',pos);
					a=true;
				}
				else{
					cout<<"El elemento ya se inserto"<<endl;
				}
				recorrerLista(lista,false);
				break;
			case 3:
				if(b==false){
					cout<<"Insertando el nodo I entre M y L"<<endl;
					pos = obtenerPosicion(lista,'L');
					insertarPosicion(lista,'A',pos);
					b=true;
				}
				else{
					cout<<"El elemento ya se inserto"<<endl;
				}
				recorrerLista(lista,false);
				break;
			case 4:
				recorrerLista(lista,true);
				break;		
		}
		opc = menu();
	}
	
	
	cin.get();
}

tLista crearNodo(char dato){
	tLista nuevo = new tNodo;
	nuevo->dato = dato;
	nuevo->ant = NULL;
	nuevo->sig = NULL;
	
	return nuevo;
}
void insertarInicio(tLista &lista, char dato){
	tLista nuevo = crearNodo(dato);
	if(lista == NULL){
		lista = nuevo;
	}else{
		nuevo->sig = lista;
		lista->ant = nuevo;
		lista = nuevo;
	}
}

void insertarPosicion(tLista &lista,char dato, int pos){
	tLista nuevo = crearNodo(dato);
	if(lista == NULL){
		cout<<"Lista vacia!"<<endl;
	}else{
		if(pos == 1){
			insertarInicio(lista,dato);
		}else{
			tLista p = lista;
			tLista ant = NULL;
			int contador = 1;
			while(p!=NULL && contador<pos){
				ant = p;
				p = p->sig;
				contador++;
			}
			if(p==NULL){
				cout<<"Posicion invalida!"<<endl;
			}else{				
				nuevo->sig = p;
				nuevo->ant = ant;
				ant->sig = nuevo;
				p->ant = nuevo;
			}
		}
		
	}	
}

int menu() {

    int opc;
    
    cout<<":::::: MENU ::::::"<<endl;
	cout<<"[1] Insertar el nodo A en la primera posicion"<<endl;
	cout<<"[2] Insertar el nodo I entre N y M"<<endl;
	cout<<"[3] Insertar el nodo A entre M y L"<<endl;
	cout<<"[4] Mostrar la lista en sentido de ida y de vuelta"<<endl;
	cout<<"[0] Salir"<<endl;
    
    do{
		cout<<"OPCION: ";
		cin>>opc;	
	}while(opc<0 || 4<opc);
		
    return  opc;
}




void recorrerLista(tLista lista,bool aux){
	tLista p = lista;
	tLista ant = NULL;
	
	while(p != NULL){
		cout<<p->dato<<"->";
		ant = p;
		p = p->sig;
	}
	cout<<"NULL"<<endl;
	
	
	if(aux==true){
		while(ant != NULL){
		cout<<ant->dato<<"->";
		ant = ant->ant;
	}
	cout<<"NULL"<<endl;
	}	
	
	cout<<endl;
}

void insertarFinal(tLista &lista, char dato){
	tLista nuevo = crearNodo(dato);
	if(lista == NULL){
		lista = nuevo;
	}else{
		tLista p = lista;
		while(p->sig != NULL){
			p = p->sig;
		}
		p->sig = nuevo;
		nuevo->ant = p;
		
	}
}

int obtenerPosicion(tLista lista,char dato){
	tLista p = lista;
	int contador = 1;
	while(p->dato!=dato){
		contador++;
		p = p->sig;
	}
	
	return contador;
	
}



















